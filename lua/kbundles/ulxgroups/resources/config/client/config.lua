KConfig{
    services = {
        ulxgroups_panel = {
            object = "ulxgroups/services/Panel",
            arguments = {
                "@security"
            },
            events = {
                {
                    name = "ulxgroups:panel",
                    event = "KCore_userbundle_groups",
                    func = "getAll"
                },
                {
                    name = "ulxgroups:recived",
                    event = "KCore_userbundle_group_recived",
                    func = "valueRecived"
                }
            }
        }
    }
}
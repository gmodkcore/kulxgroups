KConfig{
    permissions = {
        ulxgroups = {
            name = "Ulx groups",
            permissions = {
                view = {
                    name = "Lista grup", -- TODO: Translations
                    description = "Może zobaczyć listę podpiętych grup" -- TODO: Translations
                },
                add = {
                    name = "Dodawanie grupy", -- TODO: Translations
                    description = "Ma możliwość dodawania grup ulx do kcore" -- TODO: Translations
                },
                remove = {
                    name = "Usuwanie grup", -- TODO: Translations
                    description = "Ma możliwość usuwania podpiętych grup" -- TODO: Translations
                }
            }
        }
    },
    translations = {
        us = {
            name = "English",
            translations = {
                ulxgroups = {
                    name = "ULX groups",
                    translations = {
                        groups = "ULX groups",
                        addgroup = "Add group"
                    }
                }
            }
        }
    }
}
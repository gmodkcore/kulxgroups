KConfig{
    services = {
        ulxgroups = {
            object = "ulxgroups/services/Groups",
            arguments = {
                "@database",
                "@user"
            },
            events = {
                {
                    name = "ulxgroups:groups",
                    event = "KCore_databaseConnected",
                    func = "databaseLoaded"
                },
                {
                    name = "ulxgroups:userchangedgroup",
                    event = "ULibUserGroupChange",
                    func = "groupChanged"
                },
                {
                    name = "ulxgroups:userremovedrole",
                    event = "ULibUserRemoved",
                    func = "groupChanged"
                }
            }
        },
        ulxgroups_admin = {
            object = "ulxgroups/services/Admin",
            arguments = {
                "@database",
                "@ulxgroups",
                "@security"
            },
            events = {
                {
                    name = "ulxgroups:request",
                    event = "KCore_config_request",
                    func = "configRequest"
                }
            }
        }
    },
    database = {
        kUserRoles = {
            fields = {
                fromulx = {
                    type = "boolean",
                    nullable = true
                }
            }
        },
        kRoles = {
            oneToMany = {
                ulxgroups = {
                    targetEntity = "kUlxGroups",
                    mappedBy = "ulxgroup"
                }
            }
        },
        kUlxGroups = {
            entity = "ulxgroups/database/UlxGroups",
            id = {
                id = {
                    type = "integer",
                    strategy = "auto"
                }
            },
            fields = {
                ulxgroup = {
                    type = "string",
                    length = "255"
                }
            },
            manyToOne = {
                group = {
                    targetEntity = "kRoles",
                    inversedBy = "ulxgroups",
                    joinColumn = {
                        name = "group_id",
                        referencedColumnName = "id"
                    }
                }
            }
        }
    }
}
local PANEL = {}

function PANEL:Paint(w, h)
    draw.RoundedBox(4, 0, 0, w, h, Color(200,200,200))
    draw.RoundedBox(4, 0, 0, w - 2, h - 2, Color(255,255,255))
end

function PANEL:Init(controller)
    self.security = controller:get("@security")
    self.translator = controller:get("@translations")

    self.groupid = self:GetParent():GetParent():GetParent():GetParent().id
    self:Dock(TOP)
    self:DockMargin(4, 0, 4, 4)

    self.top = KCore.views:create("DPanel", self)
    self.top:Dock(TOP)
    self.top.Paint = nil
    self.top:DockMargin(0,0,0,5)
    self.top.label = KCore.views:create("DLabel", self.top)
    self.top.label:Dock(LEFT)
    self.top.label:SetColor(Color(90,90,90))
    self.top.label:SetText(self.translator:trans("ulxgroups.groups"))
    self.top.label:SetFont("KCore-Roboto-20")
    self.top.label:DockMargin(5,5, 0,0)
    self.top.label:SizeToContentsX()
end

function PANEL:reposAdd()
    if not IsValid(self.addSub) then return false end
    self.addSub:InvalidateLayout(false)
end

function PANEL:SetValue(val)
    if self.security:hasPermission(LocalPlayer(), "ulxgroups.add") then
        self.add = KCore.views:create("DButton", self.top)
        self.add:Dock(RIGHT)
        self.add:SetWide(26)
        self.add:DockMargin(0, 6, 5, 0)
        self.add.Paint = nil
        self.add:SetFont("KCore-Awesome-16")
        self.add:SetText("")
        self.add:SetColor(Color(69, 145, 56))
        self.add.DoClick = function()
            if self.addSub then
                self.addSub:Remove()
                self.addSub = nil
                return
            end

            self.addSub = KCore.views:create("DPanel", self:GetParent():GetParent())
            self.addSub:SetSize(160,100)
            self.addSub.Paint = function(_, w, h)
                draw.RoundedBox(4, 0, 0, w, h, Color(200,200,200))
                draw.RoundedBox(4, 1, 1, w - 3, h - 3, Color(245,245,245))
            end

            self.addSub.info = KCore.views:create("DLabel", self.addSub)
            self.addSub.info:Dock(TOP)
            self.addSub.info:SetText(self.translator:trans("ulxgroups.addgroup"))
            self.addSub.info:SetFont("KCore-Roboto-14")
            self.addSub.info:SetColor(Color(120,120,120))
            self.addSub.info:DockMargin(5,5,0,5)

            self.addSub.info.scroll = KCore.views:create("KConfig_scroll", self.addSub)
            self.addSub.info.scroll:Dock(FILL)
            self.addSub.info.scroll:DockMargin(0,0,2,2)

            local function roleCreated(role)
                for i,k in pairs(self.list or {}) do
                    if IsValid(k) and k.command == role then return true end
                end

                return false
            end

            for i,k in pairs(ULib.ucl.groups) do
                if i == "user" or i == "noaccess" or roleCreated(i) then continue end
                self.addSub.info.scroll[i] = KCore.views:create("DPanel", self.addSub.info.scroll)
                self.addSub.info.scroll[i]:Dock(TOP)
                self.addSub.info.scroll[i].Paint = function(_, w, h)
                    draw.SimpleText(i, "KCore-Roboto-12", 25, h / 2 - 1, Color( 120, 120, 120, 255 ), TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER)
                end
                self.addSub.info.scroll[i]:SetTall(16)

                self.addSub.info.scroll[i].add = KCore.views:create("DButton", self.addSub.info.scroll[i])
                self.addSub.info.scroll[i].add:Dock(LEFT)
                self.addSub.info.scroll[i].add:DockMargin(5, 2, 5, 2)
                self.addSub.info.scroll[i].add:SetWide(16)
                self.addSub.info.scroll[i].add.Paint = nil
                self.addSub.info.scroll[i].add:SetFont("KCore-Awesome-12")
                self.addSub.info.scroll[i].add:SetText("")
                self.addSub.info.scroll[i].add:SetColor(Color(69, 145, 56))
                self.addSub.info.scroll[i].add.DoClick = function()
                    self:addRole(i)
                    self.addSub.info.scroll[i]:Remove()
                    net.Start("KCore_configsender")
                        net.WriteString("groups")
                        net.WriteTable({
                            name = "addUlxGroup",
                            group = self.groupid,
                            command = i
                        })
                    net.SendToServer()
                end
            end

            self.addSub.PerformLayout = function(nt)
                nt = nt or false
                local function repos()
                    if not IsValid(self.addSub) then return end
                    local posx, posy = self:GetPos()
                    posx = posx + self:GetWide() - self.addSub:GetWide() - 5
                    posy = posy + 24
                    self.addSub:SetPos(posx, posy)
                end
                if nt then
                    timer.Simple(0, function()
                        repos()
                    end)
                else
                    repos()
                end
            end

            self:reposAdd(true)
        end
    end

    self:InvalidateLayout(true)
    self:SizeToChildren(false, true)

    if self:GetTall() < 30 then
        self:SetTall(80)
    end

    self.list = {}

    for i,k in pairs(val or {}) do
        self:addRole(k.ulxgroup)
    end
end

function PANEL:addRole(ulxgroup)
    local function resizePanel()
        self:InvalidateChildren(true)
        self:SizeToChildren(false, true)
    end
    local role = KCore.views:create("DPanel", self)
    role:Dock(TOP)
    role.Paint = function(_, w, h)
        draw.RoundedBox(0, 0, 0, w - 2, 1, Color(220,220,220))
    end
    role.command = ulxgroup
    role:SetTall(26)

    role.name = KCore.views:create("DLabel", role)
    role.name:Dock(FILL)
    role.name:DockMargin(15, 0, 0, 0)
    role.name:SetFont("KCore-Roboto-13")
    role.name:SetColor(Color(120,120,120))
    role.name:SetText(ulxgroup)

    if self.security:hasPermission(LocalPlayer(), "ulxgroups.remove") then
        role.remove = KCore.views:create("DButton", role)
        role.remove:Dock(RIGHT)
        role.remove:SetWide(20)
        role.remove:DockMargin(0, 3, 8, 3)
        role.remove.Paint = nil
        role.remove:SetFont("KCore-Awesome-14")
        role.remove:SetText("")
        role.remove:SetColor(Color(214, 25, 25))
        role.remove.DoClick = function()
            net.Start("KCore_configsender")
                net.WriteString("groups")
                net.WriteTable({
                    name = "removeUlxGroup",
                    group = self.groupid,
                    command = ulxgroup
                })
            net.SendToServer()
            role:Remove()
            resizePanel()
        end
    end

    resizePanel()

    table.insert(self.list, role)
end

function PANEL:PerformLayout()
    self:reposAdd()
end

KCore.views:register("KConfig_ulxgroups", PANEL, "DPanel")
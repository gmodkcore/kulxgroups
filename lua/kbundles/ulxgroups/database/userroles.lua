local USERROLES = {}

USERROLES.fromulx = nil

function USERROLES:getFromulx()
    return self.fromulx
end

function USERROLES:setFromulx(fu)
    self.fromulx = fu
end

KCore.objects:createClass({
    name = "UserRoles",
    namespace = "userbundle/database",
    class = USERROLES
})
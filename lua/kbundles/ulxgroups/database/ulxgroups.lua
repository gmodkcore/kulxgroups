local GROUPS = {}

GROUPS.id = nil
GROUPS.ulxgroup = nil
GROUPS.group = nil

function GROUPS:getId()
    return self.id
end

function GROUPS:setId(id)
    self.id = id
end

function GROUPS:getUlxgroup()
    return self.ulxgroup
end

function GROUPS:setUlxgroup(group)
    self.ulxgroup = group
end

function GROUPS:getGroup()
    return self.group
end

function GROUPS:setGroup(gr)
    self.group = gr
end

KCore.objects:createClass({
    name = "UlxGroups",
    namespace = "ulxgroups/database",
    class = GROUPS
})
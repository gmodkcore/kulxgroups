local ADMIN = {}

ADMIN.db = nil
ADMIN.ulxgroups = nil
ADMIN.security = nil

function ADMIN:__construct(db, ulxgroups, security)
    self.db = db
    self.ulxgroups = ulxgroups
    self.security = security
end

function ADMIN:configRequest(ply, name, val)
    if name == "groups" and val.name then
        if val.name == "getUlxGroups" then
            self:getUlxGroups(ply, val.group)
        elseif val.name == "removeUlxGroup" then
            self:removeGroup(ply, val.group, val.command)
        elseif val.name == "addUlxGroup" then
            self:addGroup(ply, val.group, val.command)
        end
    end
end

function ADMIN:getUlxGroups(ply, group)
    if not self.security:hasPermission(ply, "ulxgroups.view") then return end
    self.db:createQuery()
        :addSelect("q")
        :from("kUlxGroups", "q")
        :where("q.group_id = :groupid")
        :setParameter("groupid", group)
        :getArrayResult(function(d)
            net.Start("KCore_configbundle_sender")
                net.WriteString("users")
                net.WriteTable({
                    type = "sendUlxGroups",
                    val = d or {}
                })
            net.Send(ply)
        end)
end

function ADMIN:addGroup(ply, groupid, command)
    if not self.security:hasPermission(ply, "ulxgroups.add") then return end
    self.ulxgroups:createGroupPin(groupid, command)
end

function ADMIN:removeGroup(ply, groupid, command)
    if not self.security:hasPermission(ply, "ulxgroups.remove") then return end
    self.db:createQuery()
        :addSelect("q")
        :from("kUlxGroups", "q")
        :addSelect("g")
        :innerJoin("q.group", "g")
        :where("q.ulxgroup = ':ulx'")
        :setParameter("ulx", command)
        :andWhere("g.id = :group")
        :setParameter("group", groupid)
        :getOneOrNullResult(function(d)
            if d == nil then return end
            self.ulxgroups:removeGroupPin(d)
        end)
end

KCore.objects:createClass({
    name = "Admin",
    namespace = "ulxgroups/services",
    class = ADMIN
})
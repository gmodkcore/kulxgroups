local GROUPS = {}

GROUPS.db = nil
GROUPS.user = nil

function GROUPS:__construct(db, user)
    self.db = db
    self.user = user
end

function GROUPS:databaseLoaded()
    if not ULib then
        print("[KCore groups] ULib not roaded - retrying")
        timer.Simple(0.1, function()
            self:databaseLoaded()
        end)
        return
    end
    self:checkGroups()
end

function GROUPS:_getAllGroups(func)
    self.db:createQuery()
        :addSelect("q")
        :from("kUlxGroups", "q")
        :addSelect("g")
        :innerJoin("q.group", "g")
        :getResult(func)
end

function GROUPS:checkGroups()
    self:_getAllGroups(function(d)
        for i,k in pairs(d) do
                if not ULib.ucl.groups[k:getUlxgroup()] then
                    self:removeGroupPin(k)
                    table.remove(d, i)
                end
            end
            self:_checkAllUsers(d)
    end)
end

function GROUPS:removeGroupPin(group)
    self.db:createQuery()
        :delete(group)
        :getResult(function()
            self:checkGroups()
        end)
end

function GROUPS:createGroupPin(groupid, ulxgroup)
    if not ULib.ucl.groups[ulxgroup] then return end
    self.db:createQuery()
        :addSelect("q")
        :from("kUlxGroups", "q")
        :where("q.group_id = :group")
        :setParameter("group", groupid)
        :andWhere("q.ulxgroup = ':ulx'")
        :setParameter("ulx", ulxgroup)
        :getArrayResult(function(d)
            if #d > 0 then return end
            self.db:createQuery()
                :addSelect("q")
                :from("kRoles", "q")
                :where("q.id = :id")
                :setParameter("id", groupid)
                :getOneOrNullResult(function(g)
                    if not g then return end
                    self:forceCreateGroupPin(groupid, ulxgroup)
                end)
        end)
end

function GROUPS:forceCreateGroupPin(groupid, ulxgroup)
    local up = KCore.new("ulxgroups/database/UlxGroups")
    up:setUlxgroup(ulxgroup)
    up:setGroup(groupid)
    self.db:createQuery()
        :prepare(up)
        :getResult(function(d)
            self:checkGroups()
        end)
end

function GROUPS:_checkAllUsers(groups)
    self.db:createQuery()
        :addSelect({"q.id", "q.steamid"})
        :from("kUser","q")
        :addSelect("ur")
        :leftJoin("q.userroles", "ur")
        :addSelect("r.id")
        :leftJoin("ur.roles", "r")
        :getResult(function(d)
            self:checkUsersGroups(groups, d)
        end)
end

function GROUPS:checkUsersGroups(ulxgroups, users)
    local avilablegroups = {}

    for i,k in pairs(ulxgroups or {}) do
        avilablegroups[k:getUlxgroup()] = avilablegroups[k:getUlxgroup()] or {}
        table.insert(avilablegroups[k:getUlxgroup()], k:getGroup():getId())
    end

    local function checkUser(user)
        local userroles = {}
        if ULib.ucl.users[user:getSteamid()] and ULib.ucl.users[user:getSteamid()].group and string.len(ULib.ucl.users[user:getSteamid()].group) > 0 then
            userroles = avilablegroups[ULib.ucl.users[user:getSteamid()].group] or {}
        end

        local userHasRoles = {}
        local countReload = 0

        local function reloadthisuser()
            countReload = countReload - 1
            if countReload <= 0 then
                self.user:loadBySteamId(user:getSteamid())
            end
        end

        for i,k in pairs(user:getRoles() or {}) do
            table.insert(userHasRoles, k:getRoles():getId())
            if k:getFromulx() and not table.HasValue(userroles, k:getRoles():getId()) then
                countReload = countReload + 1
                self.db:createQuery()
                    :delete(k)
                    :getResult(function()
                        reloadthisuser()
                    end)
            end
        end

        for i,k in pairs(userroles) do
            if not table.HasValue(userHasRoles, k) then
                local newrole = KCore.new("userbundle/database/UserRoles")
                newrole:setFromulx(true)
                newrole:setUser(user:getId())
                newrole:setRoles(k)

                countReload = countReload + 1

                self.db:createQuery()
                    :prepare(newrole)
                    :getResult(function()
                        reloadthisuser()
                    end)
            end
        end
    end

    for i,k in pairs(users) do
        checkUser(k)
    end
end

function GROUPS:groupChanged(id)
    local q = self.db:createQuery()
        :addSelect({"q.id", "q.steamid"})
        :from("kUser","q")
        :addSelect("ur")
        :leftJoin("q.userroles", "ur")
        :addSelect("r.id")
        :leftJoin("ur.roles", "r")
        if string.sub(id, 1, 6) == "STEAM_" then
            q = q:where("q.steamid = ':steamid'")
                :setParameter("steamid", id)
        else
            q = q:where("q.uniqueid = ':uid'")
                :setParameter("uid", id)
        end
        q:getResult(function(d)
            if #d == 0 then return end
            self:_getAllGroups(function(g)
                self:checkUsersGroups(g, d)
            end)
        end)
end

KCore.objects:createClass({
    name = "Groups",
    namespace = "ulxgroups/services",
    class = GROUPS
})
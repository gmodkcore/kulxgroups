local PANEL = {}

PANEL.security = nil

function PANEL:__construct(security)
    self.security = security
end

function PANEL:getAll(val)
    if not self.security:hasPermission(LocalPlayer(), "ulxgroups.view") then return end
    net.Start("KCore_configsender")
        net.WriteString("groups")
        net.WriteTable({
            name = "getUlxGroups",
            group = val.id
        })
    net.SendToServer()
end

function PANEL:valueRecived(pnl, val)
    if val.type ~= "sendUlxGroups" then return end
    if not pnl or not pnl.main or not pnl.main.box or not pnl.main.box.left then return end
    local n = KCore.views:create("KConfig_ulxgroups", pnl.main.box.left)
    n:SetValue(val.val)
end

KCore.objects:createClass({
    name = "Panel",
    namespace = "ulxgroups/services",
    class = PANEL
})